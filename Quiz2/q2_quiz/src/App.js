
import './App.css';

import Header from './components/Header';
import { Routes, Route } from "react-router-dom";
import Page1 from './page/Page1';


import GradePage from './page/GradePage';

function App() {
  return (
    <div className="App">
      <Header />
      <Routes>
          <Route path="a1" element ={
              <GradePage/>
          }/>

          <Route path="a2" element ={
              <Page1/>
          }/>

         

     
        
      </Routes>
    
    </div>
  );
}

export default App;
