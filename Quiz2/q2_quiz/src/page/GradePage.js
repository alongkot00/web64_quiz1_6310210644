
import {useState} from "react";
import Gg from "../components/Gg";

function GradePage() {

    const [ name, setName ] = useState("");
    const [score, setScore] = useState("");
    const [ translateResult, setTranslateResult ] = useState("");

 

  
    function calculatGrad(){
        let g = parseFloat(score);
        setScore(g);

        if((g>100)||(g<0)) {    
            setTranslateResult("ไม่สามารถคำนวณได้");
         }
         else if ((g>=79.5)&&(g<=100)) {    
            setTranslateResult("เอาไปเลย A");
         }
          else if ((g>=74.5)&&(g<=79.4)) {    
            setTranslateResult("เอาไปเลย B+");
         }
          else if ((g>=69.5)&&(g<=74.4)) {       
            setTranslateResult("เอาไปเลย B");
         }
          else if ((g>=64.5)&&(g<=69.4)) {
            setTranslateResult("ไเอาไปเลย C+");
         }
          else if ((g>=59.5)&&(g<=64.4)) {    
            setTranslateResult("เอาไปเลย C");
         }
          else if ((g>=54.5)&&(g=59.4)) {            
            setTranslateResult("เอาไปเลย D+");
         }
          else if ((g>=35.1)&&(g<=54.4)) {       
            setTranslateResult("เอาไปเลย D"); 
         }
          else if (g<=35) {       
            setTranslateResult("เอาไปเลย E");
         }   
    }

    return(
        <div align = "left">
            <div align = "center">
                <br/>
                <h1>คำนวณเกรดเฉลี่ย</h1>
                <br/>
    
                <h2>
                    ชื่อ : <input type = "text" value={name} onChange={(e) => { setName(e.target.value);}}/>
                </h2>
    
                <h3
                >Grade : <input type = "text" value={score} onChange={(e) =>{ setScore(e.target.value);}}/>
                </h3>

                <br />
                <button onClick={(e) => {calculatGrad()}}>คำนวณ</button>
                <br/>
    
                {  score !== 0 && 
                    <div>
                        <h3>ผลคำนวณของคุณ</h3>
                        <Gg
                            name ={ name }
                            grade = {translateResult}
                        />
                    </div>
                }
                        
            </div>
        </div>
        );





}


export default GradePage;