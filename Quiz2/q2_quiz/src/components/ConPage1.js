import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
function ConPage1 (props){


    return (
        <Box sx={ {width: "60%" }}>
       <Paper elevation={3} >
            <h2> Developed by { props.name } </h2>
            
            <h3> He is from { props.province }.</h3>
            </Paper>
</Box>

    );
}

export default ConPage1;